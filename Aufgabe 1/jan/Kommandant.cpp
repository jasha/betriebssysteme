#include <string.h>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include "simProzess.h"
using namespace std;

/*
 * 
 */
enum ausfuehrbare_modien {
    debug,
    automatic
};

void printprozesse(vector <simProzess*> &prozesstabelle) {

    cout << endl << "****************************************************************" << endl;
    cout << "The current system state is as follows:" << endl;
    cout << "****************************************************************" << endl;
    cout << "CURRENT TIME: 69" << endl;
    cout << "RUNNING PROCESS:" << endl;
    cout << "pid  ppid  value  time used so far<<endl;" << endl;

    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == aktiv) {
            cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << endl;
        }
    }
    cout << "BLOCKED PROCESSES:" << endl;
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == blockiert) {
            cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << endl;
        }
    }

    //tstweise
    cout << "ALLE prozesse" << endl;
    for (int i = 0; i < prozesstabelle.size(); i++) {
        cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << " Status: " << prozesstabelle[i]->GetStatus() << endl;
    }


}

void prozessinit(vector <simProzess*> &prozesstabelle) {

    simProzess *s1 = new simProzess;
    s1->SetDateiname("init");
    prozesstabelle.push_back(s1);


}

void prozesserzeugen(vector <simProzess*> &prozesstabelle, string prozessname) {
    simProzess *s1 = new simProzess;
    s1->SetDateiname(prozessname);
    prozesstabelle.push_back(s1);
}

void prozessausfuehren(int &r0, int &pc, simProzess *obj, vector <simProzess*> &prozesstabelle, queue <simProzess*> &warteschlange) {
    

    ifstream eingabe;
    eingabe.open(obj->GetDateiname().c_str(), ios::in);
    if (!eingabe) { // Datei kann nicht geoeffnet werden
        cerr << "Datei kann nicht geöffnet werden bzw. ist nicht vorhanden!\n";
        return;
    }

    //Lade Werte des Prozesses
    r0 = obj->GetGr0();
    pc = obj->GetGpc();


    string zspeicher; //zwischenspeicher für eingelesene Befehle
    string inputBefehl;
    int inputn;
    string inputfilename;

    for (int i = 0; i < pc + 1; i++) //solang einlesen bis aktuelle Befehl ausgewählt wurde
        getline(eingabe, zspeicher);

    inputBefehl = zspeicher.substr(0, 1); //Zeichen trennen Bsp. (A 20) -> A
    zspeicher.erase(0, 2);

    if (eingabe.fail()) {
        inputBefehl = 'E'; //Prozess beenden
    }

    if (inputBefehl == "S" || inputBefehl == "A" || inputBefehl == "D") {
        stringstream Str(zspeicher);
        Str >> inputn;
    }
    if (inputBefehl == "R")
        inputfilename = zspeicher;



    switch (inputBefehl.c_str()[0]) {
        case('s'):
        case('S'): r0 = inputn;
            break;
        case('a'):
        case('A'): r0 = r0 + inputn;
            break;
        case('d'):
        case('D'): r0 = r0 - inputn;
            break;
        case('b'):
        case('B'):obj->SetStatus(blockiert);
            warteschlange.push(obj);
            break;
        case('e'):
        case('E'):obj->SetStatus(beendet);
            eingabe.close();
            break;
        case('r'):
        case('R'):obj->SetStatus(blockiert);
            warteschlange.push(obj);
            prozesserzeugen(prozesstabelle, inputfilename);
            break;
        default:break;
    }

    //Werte speichern
    obj->SetGr0(r0);
    obj->SetGpc(++pc);


}

void prozessauswahl(vector <simProzess*> &prozesstabelle, int &r0, int &pc, queue<simProzess*> &warteschlange,int &modus) {

    int aktiveprozesse = 0;

    //Schauen ob es aktive Prozesse gibt
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == aktiv)
            aktiveprozesse++;
    }

    //aktiven prozess ausfuehren wenn vorhanden
    if (aktiveprozesse > 0)
        for (int i = 0; i < prozesstabelle.size(); i++) {
            if (prozesstabelle[i]->GetStatus() == aktiv)
                prozessausfuehren(r0, pc, prozesstabelle[i], prozesstabelle, warteschlange);
        }
    //wenn kein aktiver Prozess vorhanden warteschlange ab arbeiten
    if (aktiveprozesse == 0) {
        if (warteschlange.empty()) {
            cout << endl << "Alle Prozesse abgeschlossen!" << endl;
            modus=debug;
            //exit(0);
        } else {
            warteschlange.front()->SetStatus(aktiv);
            prozessausfuehren(r0, pc, warteschlange.front(), prozesstabelle, warteschlange);
            warteschlange.pop();
        }

    }
}

void befehlsauswertung(char const var[], int &r0, int &pc, vector <simProzess*> &prozesstabelle, queue <simProzess*> &warteschlange, int &modus) {

    
    if (strcmp(var, "Q") == 0 || strcmp(var, "Quit") == 0) {

        float dplaufzeit = 0; //Durchschnittliche Prozesslaufzeit var

        for (int i = 0; i < prozesstabelle.size(); i++) {
            dplaufzeit = dplaufzeit + prozesstabelle[i]->GetGpc();
        }
        dplaufzeit = dplaufzeit / prozesstabelle.size();
        cout << endl << "Durchschnittliche Prozesslaufzeit: " << dplaufzeit << endl;



        exit(0);
    } else if (strcmp(var, "P") == 0 || strcmp(var, "Print") == 0) {
        int reportPid = fork();

        if (reportPid == 0) {

            printprozesse(prozesstabelle);
            exit(0);

        } else {

            wait(NULL);
            //waitpid(reportPid, NULL, 0);
            //printf("reportProzess %i zu ende", reportPid);
        }

    } else if (strcmp(var, "Step") == 0 || strcmp(var, "step") == 0) {

        prozessauswahl(prozesstabelle, r0, pc, warteschlange,modus);


    }
    else if (strcmp(var, "U") == 0 || strcmp(var, "Unblock") == 0) {

        for (int i = 0; i < prozesstabelle.size(); i++) {
            if (prozesstabelle[i]->GetStatus() == aktiv)
                prozesstabelle[i]->SetStatus(blockiert);
            warteschlange.push(prozesstabelle[i]);
        }
        warteschlange.front()->SetStatus(aktiv);

    }
    else if (strcmp(var, "M") == 0 || strcmp(var, "Mode") == 0) {
        modus = 1;
    }
    else printf("\n Parent’s PID: %d - Befehlsauswertung = %s\n", getppid(), var);

}

int main(int argc, char** argv) {

    //Prozessmanagervariablen
    vector <simProzess*> prozesstabelle;
    queue <simProzess*> warteschlange;
    prozessinit(prozesstabelle);    //erstellt prozess mit init datei
    int modus = debug;
    int r0, pc;
    char rarray[255];
    //--

    int status = 0;
    char warray[255];
    int fd[2];
    pipe(fd); //0 = Lesen , 1= Schreiben

    int childPid = fork();


    if (pipe < 0) {
        printf("Fehler beim erstellen der Pipe");
        return 0;
    }






           
                printf("\n Parentprozess/Kommandant \n");
                //close(fd[0]); //Leseseite der Pipe schließen
                printf("Gebe eine Befehl ein: ");
                scanf("%254s", &warray[0]);
                write(STDOUT_FILENO, warray, sizeof (warray));

                if (strcmp(warray, "Q") == 0 || strcmp(warray, "Quit") == 0) {
                    wait(&status);
                    break;
                }              

        

        fflush(stdout);
        fflush(stdin);
    }
    printf("\n BYE \n ");
    return 0;
}

