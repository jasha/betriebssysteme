/* 
 * File:   simProzess.h
 * Author: debian
 *
 * Created on 23. April 2017, 15:14
 */
using namespace std;
#include <string>

#ifndef SIMPROZESS_H
#define	SIMPROZESS_H

	enum status {
		aktiv,
		blockiert,
		beendet,
	};

class simProzess {
public:
    simProzess();
    simProzess(const simProzess& orig);
    virtual ~simProzess();
    void SetGpc(int gpc);
    int GetGpc() const;
    void SetGr0(int gr0);
    int GetGr0() const;
    void SetStatus(int status);
    int GetStatus() const;
    int GetId() const;
    void SetDateiname(string dateiname);
    string GetDateiname() const;
	int id;
        int pid;
        static int counter;
        

        void speichern();
        void SetPid(int pid);
        int GetPid() const;
private:
    
	int status;	//Aktiv oder angehalten
	int gr0;		//gespeichertes r0
	int gpc;		//programmcounter
        string dateiname;
};

#endif	/* SIMPROZESS_H */

