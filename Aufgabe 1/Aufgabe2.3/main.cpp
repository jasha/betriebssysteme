/* 
 * File:   main.cpp
 * Author: debian
 *
 * Created on 15. April 2017, 15:33
 */
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <queue>
#include "simProzess.h"
using namespace std;

/*
 * 
 */


static volatile sig_atomic_t globalFlag = 0;

void signal_handler(int signal) {
    globalFlag = 1;
    return;
}

enum ausfuehrbare_modien {
    debug,
    automatic
};

void printprozesse(vector <simProzess*> &prozesstabelle) {

    cout << endl << "****************************************************************" << endl;
    cout << "The current system state is as follows:" << endl;
    cout << "****************************************************************" << endl;
    cout << "CURRENT TIME: 69" << endl;
    cout << "RUNNING PROCESS:" << endl;
    cout << endl << "pid  ppid  value  time used so far" << endl;

    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == aktiv) {
            cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << endl;
        }
    }

    cout << "PROCESSES READY TO EXECUTE:" << endl;
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == bereit) {
            cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << endl;
        }
    }



    cout << "BLOCKED PROCESSES:" << endl;
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == blockiert) {
            cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << endl;
        }
    }

    //tstweise
    cout << "ALLE prozesse" << endl;
    for (int i = 0; i < prozesstabelle.size(); i++) {
        cout << prozesstabelle[i]->id << "    " << prozesstabelle[i]->pid << "     " << prozesstabelle[i]->GetGr0() << "      " << prozesstabelle[i]->GetGpc() + 1 << " Status: " << prozesstabelle[i]->GetStatus() << endl;
    }


}

void prozessinit(vector <simProzess*> &prozesstabelle) {

    simProzess *s1 = new simProzess;
    s1->SetDateiname("init");
    prozesstabelle.push_back(s1);


}

void prozesserzeugen(vector <simProzess*> &prozesstabelle, string prozessname,int pid) {
    simProzess *s1 = new simProzess;
    s1->SetDateiname(prozessname);
    s1->pid=pid;
    prozesstabelle.push_back(s1);
     cout<<endl<<"Prozess wechsel. ID: "<< s1->GetId()<<endl;
}

void prozessausfuehren(int &r0, int &pc, simProzess *obj, vector <simProzess*> &prozesstabelle, queue <simProzess*> &warteschlange) {


    ifstream eingabe;
    eingabe.open(obj->GetDateiname().c_str(), ios::in);
    if (!eingabe) { // Datei kann nicht geoeffnet werden
        cerr << "Datei kann nicht geöffnet werden bzw. ist nicht vorhanden!\n";
        return;
    }

    //Lade Werte des Prozesses
    r0 = obj->GetGr0();
    pc = obj->GetGpc();


    string zspeicher; //zwischenspeicher für eingelesene Befehle
    string inputBefehl;
    int inputn;
    string inputfilename;

    for (int i = 0; i < pc + 1; i++) //solang einlesen bis aktuelle Befehl ausgewählt wurde
        getline(eingabe, zspeicher);

    inputBefehl = zspeicher.substr(0, 1); //Zeichen trennen Bsp. (A 20) -> A
    zspeicher.erase(0, 2);

    if (eingabe.fail()) {
        inputBefehl = 'E'; //Prozess beenden
    }

    if (inputBefehl == "S" || inputBefehl == "A" || inputBefehl == "D") {
        stringstream Str(zspeicher);
        Str >> inputn;
    }
    if (inputBefehl == "R")
        inputfilename = zspeicher;



    switch (inputBefehl.c_str()[0]) {
        case('s'):
        case('S'): r0 = inputn;
            break;
        case('a'):
        case('A'): r0 = r0 + inputn;
            break;
        case('d'):
        case('D'): r0 = r0 - inputn;
            break;
        case('b'):
        case('B'):obj->SetStatus(blockiert);
            break;
        case('e'):
        case('E'):obj->SetStatus(beendet);
            eingabe.close();
            break;
        case('r'):
        case('R'):obj->SetStatus(bereit);
            warteschlange.push(obj);
            prozesserzeugen(prozesstabelle, inputfilename,obj->GetId());
            break;
        default:break;
    }

    //Werte speichern
    obj->SetGr0(r0);
    obj->SetGpc(++pc);


}

void prozessauswahl(vector <simProzess*> &prozesstabelle, int &r0, int &pc, queue<simProzess*> &warteschlange, int &modus) {

    int aktiveprozesse = 0;

    //Schauen ob es aktive Prozesse gibt
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == aktiv)
            aktiveprozesse++;
    }

    //aktiven prozess ausfuehren wenn vorhanden
    if (aktiveprozesse > 0)
        for (int i = 0; i < prozesstabelle.size(); i++) {
            if (prozesstabelle[i]->GetStatus() == aktiv) {
                prozessausfuehren(r0, pc, prozesstabelle[i], prozesstabelle, warteschlange);
                break;
            }
        }
    //wenn kein aktiver Prozess vorhanden warteschlange ab arbeiten
    if (aktiveprozesse == 0) {
        if (warteschlange.empty()) {
            cout << endl << "Alle Prozesse abgeschlossen!" << endl;
            modus = debug;
            //exit(0);
        } else {
            cout<<endl<<"Prozess wechsel. ID: "<< warteschlange.front()->GetId()<<endl;
            warteschlange.front()->SetStatus(aktiv);
            prozessausfuehren(r0, pc, warteschlange.front(), prozesstabelle, warteschlange);
            warteschlange.pop();
        }

    }
}

void prozessauswahlquantum(vector <simProzess*> &prozesstabelle, int &r0, int &pc, queue<simProzess*> &warteschlange, int &modus) {

    int aktiveprozesse = 0;

    //Schauen ob es aktive Prozesse gibt
    for (int i = 0; i < prozesstabelle.size(); i++) {
        if (prozesstabelle[i]->GetStatus() == aktiv)
            aktiveprozesse++;
    }

    //aktiven prozess ausfuehren wenn vorhanden
    if (aktiveprozesse > 0)
        for (int i = 0; i < prozesstabelle.size(); i++) {
            if (prozesstabelle[i]->GetStatus() == aktiv) {

                if ((prozesstabelle[i]->GetGpc() + 1) % 3 == 0) {
                    prozesstabelle[i]->SetStatus(bereit);
                    warteschlange.push(prozesstabelle[i]);
                } else
                    prozessausfuehren(r0, pc, prozesstabelle[i], prozesstabelle, warteschlange);
            }
        }
    //wenn kein aktiver Prozess vorhanden warteschlange ab arbeiten
    if (aktiveprozesse == 0) {
        if (warteschlange.empty()) {
            cout << endl << "Alle Prozesse abgeschlossen!" << endl;
            modus = debug;

        } else {
            cout<<endl<<"Prozess wechsel. ID: "<< warteschlange.front()->GetId()<<endl;
            warteschlange.front()->SetStatus(aktiv);
            prozessausfuehren(r0, pc, warteschlange.front(), prozesstabelle, warteschlange);
            warteschlange.pop();
        }

    }
}

void befehlsauswertung(char const var[], int &r0, int &pc, vector <simProzess*> &prozesstabelle, queue <simProzess*> &warteschlange, int &modus) {


    if (strcmp(var, "Q") == 0 || strcmp(var, "Quit") == 0) {

        float dplaufzeit = 0; //Durchschnittliche Prozesslaufzeit var

        for (int i = 0; i < prozesstabelle.size(); i++) {
            dplaufzeit = dplaufzeit + prozesstabelle[i]->GetGpc();
        }
        dplaufzeit = dplaufzeit / prozesstabelle.size();
        cout << endl << "Durchschnittliche Prozesslaufzeit: " << dplaufzeit << endl;



        exit(0);
    } else if (strcmp(var, "P") == 0 || strcmp(var, "Print") == 0) {
        int reportPid = fork();

        if (reportPid == 0) {

            printprozesse(prozesstabelle);
            exit(0);

        } else {
            wait(NULL);
        }

    } else if (strcmp(var, "Step") == 0 || strcmp(var, "step") == 0) {

        prozessauswahl(prozesstabelle, r0, pc, warteschlange, modus);


    } else if (strcmp(var, "U") == 0 || strcmp(var, "Unblock") == 0) {

        for (int i = prozesstabelle.size() - 1; i >= 0; i--) {
            if (prozesstabelle[i]->GetStatus() == blockiert) {
                prozesstabelle[i]->SetStatus(bereit);
                warteschlange.push(prozesstabelle[i]);
                break;
            }
        }


    } else if (strcmp(var, "M") == 0 || strcmp(var, "Mode") == 0) {
        if (modus == 0)
            modus = 1;
        else
            modus = 0;
    } else cout << endl << "Fehlerhafter Befehl!" << endl;

}

int main(int argc, char** argv) {

    //Prozessmanagervariablen
    vector <simProzess*> prozesstabelle;
    queue <simProzess*> warteschlange;
    prozessinit(prozesstabelle); //erstellt prozess mit init datei
    int modus = debug;
    int r0, pc;
    char rarray[255];
    //--

    int status = 0;
    char warray[255];
    int fd[2];
    pipe(fd); //0 = Lesen , 1= Schreiben


    if (pipe < 0) {
        printf("Fehler beim erstellen der Pipe");
        return 0;
    }


    printf("\n Programm bereit zur eingabe.\n");

    int childPid = fork();





    while (true) {

        if (childPid == -1) {
            printf(" fehler beim erstellen des Kindporzesses \n");
            exit(-1);
        }


        if (childPid == 0) {
            signal(SIGUSR1, signal_handler);


            if (modus == debug || globalFlag == 1) {
                close(fd[1]); //Schreibseite der Pipe schließen
                read(fd[0], rarray, sizeof (rarray));
                befehlsauswertung(rarray, r0, pc, prozesstabelle, warteschlange, modus);
                globalFlag = 0;
            }
            if (modus == automatic) {
                cout << ".";
                befehlsauswertung("step", r0, pc, prozesstabelle, warteschlange, modus);
                sleep(1);
            }
        }

        if (childPid > 0) {

            close(fd[0]); //Leseseite der Pipe schließen
            scanf("%254s", &warray[0]);
            write(fd[1], warray, sizeof (warray));
            kill(childPid, SIGUSR1);
            if (strcmp(warray, "Q") == 0 || strcmp(warray, "Quit") == 0) {
                wait(&status);
                break;
            } //wenn exit geschrieben wurde auf Kindprozess beendigung warten                

        }

        fflush(stdout);
        fflush(stdin);
    }
    printf("\n BYE \n ");
    return 0;
}

